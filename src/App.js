import React from 'react';
import './App.css';
import  "semantic-ui-css";

import Footer from "./Component/footer/footer";
import  Header from "./Component/header/header";
import DynaRouter from "./Component/router/router";

function App() {

  return (
    <div className="App">
      <div className="ui padded grid">
  <div className=" row">
    <div class="column">
      <Header/> </div>
  </div>
  <div className="row">
    <div className="column">
      <DynaRouter/>
      </div>
  </div>
  <div className="row">
    <div className="column">   
      <Footer/></div>
  </div>
  </div>
      

  
    </div>
  );
}

export default App;
