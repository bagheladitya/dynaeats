
import React, { Component } from 'react';
import data from "../mockData/vendor1.json";
import Manager from "../Component/user/manager"


export class  Dashboard extends Component {
    
     constructor(){
  
          super()
          
         let token = localStorage.getItem('token');
         let login
         if(token==null){
             login=false
         }else{
            login=true;
         }
         this.state={login}
     }
  
    render(){
    
        return (
        
            (this.state.login) ? (<div>
                       
                <div className="ui  container padded ">

                    <Manager data={data} userType={this.props.userType} access ={this.props.access} />

                </div>

            </div>) : (<div>Access Denied !</div>)
        )
    }
}
