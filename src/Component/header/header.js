import React from 'react';
import { Component } from 'react';
 
export default class Header extends Component {
    constructor(props){
        super(props)
        let token = localStorage.getItem('token');
        let login;
        if(token==null){
            login=false;
        }
        else{
            login=true;
        }
        this.state={
            login
        }


    }
    componentDidMount(){
        let token = localStorage.getItem('token');
        if (token == null) {
            token = false;
        }
        this.setState({ token: token })
    }
     render(){
        

         return (<div>
             <div className="ui  inverted right menu">
                 <div className="item">
                     DynaEats
        </div>

                 {((!this.state.login))? 
                     (<a className="item  primary right" href='/login'>Sign-In</a>):
                     (<a className="item  primary right" href='/logout'>Sign-Out</a>) 
                     }
                      

             </div>
         </div>


         )
     }
  
  }
