

import React from 'react';
export default function Manager(props) {
    return (
      Object.keys(props.data).map(function(e){
          return  ( <div> <br/><h3 class="ui block header">
          {e}
        </h3>
        {row(props.data[e], props.userType , props.access)} </div> )
    })
    )
}

 const row = (e ,userType,access)=>{
  

    return (
         <table className="ui inverted grey table">
        <thead>
        <tr><th>Ingredient Name</th>
        {(userType === 'manager')?(<th>Required Qty</th> ):('')}
        {(userType === 'manager') ? (<th>Vendor1</th>) : ('')}
        {(userType === 'manager') ? (<th>Vendor2</th>) : ('')}
        {(userType === 'manager') ? (<th>Recieved Qty</th>) : ('')}
        <th>status</th>
        {(userType === 'manager' || access === 'bakery') ? (<th>Bakery</th>) : ('')}
        {(userType === 'manager' || access === 'italian') ? ( <th>Italian</th>) : ('')}
        {(userType === 'manager' || access === 'indian') ? (<th>Indian</th>) : ('')}
          
            
           
            
        </tr>
    </thead>
        <tbody>
    { e.map(t => { 
           if(userType=='manager'){
            return (<tr key={t}>
            <td>{t.name}</td>
            {(userType === 'manager') ? (<td>{t.required}</td>) : ('')}
            {(userType === 'manager') ? (<td>{t.qty[0]}</td>) : ('')}
            {(userType === 'manager') ? (<td>{t.qty[1]}</td>) : ('')}
            {(userType === 'manager') ? (<td>{t.qty[0] + t.qty[0]} </td>) : ('')}
             <td>  {(t.required <= (t.qty[0] + t.qty[0]) ? "Sufficient" : "Insufficient")}</td>
            <td>   {(t.bakery) ? (<i class="check  icon"></i>) : (<i class="x icon"></i>)}</td>
            <td>   {(t.italian) ? (<i class="check  icon"></i>) : (<i class="x icon"></i>)}</td>
            <td>   {(t.indian) ? (<i class="check  icon"></i>) : (<i class="x icon"></i>)}</td>
            </tr>)
           }else{
               return (<tr key={t}>
                <td>{t.name}</td>
                {(userType === 'manager') ? (<td>{t.required}</td>) : ('')}
                {(userType === 'manager') ? (<td>{t.qty[0]}</td>) : ('')}
                {(userType === 'manager') ? (<td>{t.qty[1]}</td>) : ('')}
                {(userType === 'manager') ? (<td>{t.qty[0] + t.qty[0]} </td>) : ('')}
        
               <td>  {(t.required <= (t.qty[0] + t.qty[0]) ? "Sufficient" : "Insufficient")}</td>
                {(access === 'bakery')?(<td>   {(t.bakery) ? (<i class="check  icon"></i>) : (<i class="x icon"></i>)}</td>):("")}
                {(access === 'italian')?( <td>  {(t.italian) ? (<i class="check  icon"></i>) : (<i class="x icon"></i>)}</td>):("")}
                 {(access === 'indian')?(  <td>  { (t.indian) ? (<i class="check  icon"></i>) : (<i class="x icon"></i>)}</td>):("")}
               
                
                </tr>
                
             ) }  })}
    </tbody>
    </table>


)
}