import React from 'react';
import { Dashboard } from '../dashboard'
import Login from "../login";
import LandingPage from '../landingPage';
import  Logout  from "../logout";
import {
  BrowserRouter ,
  Switch,
  Route,
} from "react-router-dom";

export default function DynaRouter() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/logout">
            <Logout />
          </Route>
      
          <Route exact
            path='/dashboard/manager'
            render={(props) => <Dashboard {...props} userType='manager' access='all' />}/>
          <Route exact
            path='/dashboard/chef/indian'
            render={(props) => <Dashboard {...props} userType='chef' access='indian'/>} />
          <Route exact
            path='/dashboard/chef/italian'
            render={(props) => <Dashboard {...props} userType='chef' access='italian' />} />
          <Route exact
            path='/dashboard/chef/bakery'
            render={(props) => <Dashboard {...props} userType='chef' access='bakery' />} />
          <Route path="/">
            <LandingPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

