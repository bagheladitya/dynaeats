import React, { Component } from "react";
// import chef from "../chef.png";
// import manager from "../manager.jpg";
// import { render } from "@testing-library/react";
import loginService from '../login/login'
import {
  Redirect
} from "react-router-dom";


export default class  Login extends Component {
 constructor(props){
   super(props)
  this.state={
    username:'',
    password:'',
    userType:'manager',
    access:'',
    login: false,
    error:false
  
  }
 }
 handleChange=(event)=>{
  const target = event.target;
  const value =  target.value;
  const name = target.name;
  this.setState({
    [name]: value
  });
 }
  onLoggin =(event)=>{
     
   event.preventDefault();
   if (this.state.username !== '' && this.state.password !== '' && this.state.userType !== ''){
     loginService(this.state.username, this.state.password, this.state.userType).then((e)=>{
     
        
          this.setState({access:e.access ,login:true})
          localStorage.setItem('token','dynaeat');
        });
   }
   else {
     this.setState({login:false})
   }
 }
 
 
  render() { 
   return (!this.state.login)?(<div class="page-login">
     <div class="ui centered grid container">
       <div class="nine wide column">
         {/* <div class="ui icon warning message">
           <i class="lock icon"></i>
            <div class="content">
             <div class="header">
               Login failed!
               </div>
              <p></p>
           </div>
         </div> */}
         <div class="ui fluid card">
           <div class="content">
             <form class="ui form">
               <div class="field">
                 <label>UserName</label>
                 <input type="text" name="username" placeholder="User" onChange={this.handleChange} />
               </div>
               <div class="field">
                 <label>Password</label>
                 <input type="password" name="password" placeholder="Password" onChange={this.handleChange} />
               </div>
               <div class="field" onChange={this.handleChange}>
                 <label>Sign in as: </label>
                 <div class="ui form">
                   <div class="grouped fields">
                     <label></label>
                     <div class="field">
                       <div class="ui radio checkbox">
                         <input type="radio" name="userType" checked={(this.state.userType === 'manager') ? 'checked' : ''} onClick={()=>{this.setState({userType:'manager'})}} value="manager" />
                         <label>Manager</label>
                       </div>
                     </div>
                     <div class="field">
                       <div class="ui radio checkbox">
                         <input type="radio" name="userType" checked={(this.state.userType === 'chef') ? 'checked' : ''} onClick={()=>{this.setState({userType:'chef'})}} value="chef" />
                         <label>Chef</label>
                       </div>
                     </div>
                   </div>
                 </div>


               </div>
               <button class="ui primary labeled icon button" type="submit" onClick={this.onLoggin}>
                 <i class="unlock alternate icon"></i>
                 Login
             </button>
             </form>
           </div>
         </div>
       </div>
     </div>
   </div>) : (<div>  
  { (this.state.userType==='manager')?
    (<Redirect to={{ pathname: `/dashboard/manager`}} />  ):
    // (<Redirect to={{pathname: `/dashboard/chef/:type`}} />) 
    (<Redirect to={{ pathname: `/dashboard/chef/${this.state.access}`}} />)
  }
   
    </div>)
}
}