import credentials from "../mockData/credentials"; 

export default function LoginService(username,password,userType){
    
    return new Promise((resolve,reject)=>{
        if (credentials.hasOwnProperty(username)){

            if (credentials[username].password === password && credentials[username].userType===userType) {
                resolve({ "message": "Logged in successfully", success: true, userType: userType, access: credentials[username].access})
            }else{
                resolve({ "message": "Wrong Password or Wrong User type", success: false })
            }
        }else{
            resolve({ "message": "Please check user not Exist", success: false })
        }
})

}